<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'assetplus');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Dfw57SWa8GlnQWaaCvSIPCPq56O0W55DZ09Xp0e8e3gwBZebiWAVSdX4GXHbsPtf');
define('SECURE_AUTH_KEY',  'ewdykG5HUKUy576OnJmovx2RZ5GhkpVMH5KKVoXOgIByUOnUDVz8RdWE5hcalZOY');
define('LOGGED_IN_KEY',    'uqOnge4sRKJgRTTI4hrCNW9MuNzpGfqj7kaQaOUaRifhSXYit2qwjJECCOHKfVPn');
define('NONCE_KEY',        'Bs3PB80zARm5hDTYvzg6tCzvB1wJhsSNjML6oJ7pLKr0QHL48J2kh6JxY6UUUw9Q');
define('AUTH_SALT',        'CHyguSymnRlbjYDh2IQAXmVR4EqORwQuXXnzxYf8K7gFhAQqFvr6zSmKkP2jOuYx');
define('SECURE_AUTH_SALT', '843JYayZpKuXaSY4e5h56n0RQf1hLUnlWmVO6oq2RQ9QeC7NdScyXyz8zpYZcPwx');
define('LOGGED_IN_SALT',   'tdHtzWLEGbnxHw5ACNFyG39egV7HCKa3mG8qmMCguOZQJ7347XxQwBBBrEcs3D6t');
define('NONCE_SALT',       'BTK8PlIKxCOBdA3khdCVCjha1KSKifMHhdcaj4nkHlfs5xIzymDkrvpiFYOm0snn');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'nptopi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');