<?php
/**
 * Template Name: Investors Page
 *
 * @package WordPress
 * @subpackage npt
 *
 */

get_header(); ?>
<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
	<?php 
	do_action( 'omega_before_content' ); 
	do_action( 'omega_content' );
	do_action( 'omega_after_content' ); 
	?>
</main><!-- .content -->
	<aside class="<?php echo omega_apply_atomic( 'sidebar_class', 'col-2 widget-area' );?>" <?php omega_attr( 'sidebar' ); ?>>

	<div class="npt-block width-50 blue-bg share-price">
		<div class="block-title entry-title">Share Price</div>
		<div class="block-content">
			$<?php $xml=simplexml_load_file("https://feeds.companyresearch.nzx.com/live_share_price.php?stock=APL"); if(isset($xml->feed)) echo $xml->feed->issuer->last; ?>
		</div>
	</div>
	<div class="npt-block width-50 blue-net-bg align-right">
		<div class="block-title entry-title" style="margin-bottom: 5px;"><a href="/latest-news" title="NZX Announcements">NZX Announcements</a></div>
		<div class="block-content"><a href="/latest-news" title="Latest News"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/line-chart.png" alt="NZX Announcements" class="img-calendar img-announcements" /></a></div>
	</div>
	<div class="npt-block width-50 blue-net-bg align-right" style="padding-bottom: 10px;">
		<div class="block-title entry-title" style="margin-bottom: 5px;"><a href="<?php echo get_permalink(210); ?>" title="Shareholder Calendar">Shareholder <br/>Calendar</a></div>
		<div class="block-content"><a href="<?php echo get_permalink(17); ?>" title="Shareholder Calendar"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/calendar.png" alt="Shareholder Calendar" class="img-calendar" /></a></div>
	</div>
	<div class="npt-block width-50 img-bg reports">
		<div class="block-title entry-title"><a href="<?php echo get_permalink(217); ?>" title="Financial Reporting">Financial <br/>Reporting</a></div>
		<div class="block-content"><a href="<?php echo get_permalink(217); ?>" title="Financial Reporting">&nbsp;</a></div>
	</div>
	<div class="npt-block width-100 img-bg">
		<div class="block-title entry-title"><a href="<?php echo get_permalink(213); ?>" title="Corporate Governance" style="padding-bottom: 70px;">Corporate <br/>Governance</a></div>
		<div class="block-content">&nbsp;</div>
	</div>
  	</aside><!-- .slideshow -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>