<?php
 /*Template Name: NPT Single Property
 */
 
get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
	<article <?php omega_attr( 'post' ); ?>>
		<div class="entry-wrap">
			<div class="entry-content" itemprop="text">
				<?php do_action( 'omega_before_entry' ); ?><!-- .page-header -->
				<div class="wide-sidebar-left">
					<?php if( get_field('property_location') ): ?><div class="property-location"><?php the_field('property_location'); ?></div><?php endif; ?>
					<div class="property-text">

                        <?php the_content(); ?>

                        <div class="head-wrapper stephen" style="display:none;">
                            <div class="head-wrapper-head">
                                <?php
                                $meta = get_post_meta( 1040 );
                                $image = wp_get_attachment_metadata($meta['staff_photo'][0]); ?>

                                    <?php if( !empty($image) ):

                                        // vars
                                        $alt = $image['image_meta']['title'];
                                        $url = $image['sizes']['thumbnail']['file'];

                                        ?>

                                        <img class="staff-actual-photo" src="/wp-content/uploads/<?php echo $url; ?>" alt="<?php echo $alt; ?>" />

                                    <?php endif; ?>

                            </div>
                            <div class="head-wrapper-content">
                                <h2>Stephen Brown-Thomas</h2>
                                <p style="margin-bottom: 0;">Property Development Manager</p>
                                <a href="mailto:stephen@augusta.co.nz">stephen@augusta.co.nz</a>
                            </div>
                        </div>

                        <div class="head-wrapper ben" style="display:none;"">
                            <div class="head-wrapper-head">
                                <?php
                                $meta = get_post_meta( 1038 );
                                $image = wp_get_attachment_metadata($meta['staff_photo'][0]); ?>

                                <?php if( !empty($image) ):

                                    // vars
                                    $alt = $image['image_meta']['title'];
                                    $url = $image['sizes']['thumbnail']['file'];

                                    ?>

                                    <img class="staff-actual-photo" src="/wp-content/uploads/<?php echo $url; ?>" alt="<?php echo $alt; ?>" />

                                <?php endif; ?>
                            </div>
                            <div class="head-wrapper-content">
                                <h2>Ben Visser</h2>
                                <p style="margin-bottom: 0;">Asset Manager</p>
                                <a href="mailto:ben@augusta.co.nz">ben@augusta.co.nz</a>
                            </div>
                        </div>

                    </div>
					<div class="property-details">
						<?php if( get_field('property_net_lettable_area') ): ?>
							<div class="property_net_lettable_area">
								<h2>Net lettable area</h2>
								<p><?php the_field('property_net_lettable_area'); ?></p>
							</div>
						<?php endif; ?>
						<?php if( get_field('property_current_market_value') ): ?>
							<div class="property_current_market_value">
								<h2>Current market value</h2>
								<p><?php the_field('property_current_market_value'); ?></p>
							</div>
						<?php endif; ?>
						<?php if( get_field('property_occupancy_by_area') ): ?>
							<div class="property_occupancy_by_area">
								<h2>Occupancy by Area</h2>
								<p><?php the_field('property_occupancy_by_area'); ?></p>
							</div>
						<?php endif; ?>
						<?php if( get_field('property_walt') ): ?>
							<div class="property_walt">
								<h2>WALT</h2>
								<p><?php the_field('property_walt'); ?></p>
							</div>
						<?php endif; ?>
						<?php if( get_field('property_major_tenants') ): ?>
							<div class="property_major_tenants">
								<h2>Major tenants</h2>
								<?php the_field('property_major_tenants'); ?>
							</div>
						<?php endif; ?>
						<?php //the_content(); ?>
					</div>
					<div class="clr"></div>
				</div>
				<div class="wide-sidebar-right">
				<div class="property-images">
					<?php $images = get_field('property_image_gallery');
					if( $images ): ?>
						<div class="property-gallery">
							<?php foreach( $images as $image ): ?>
								<img src="<?php echo $image['sizes']['npt-property-main-img-size']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endforeach; ?>
						</div>
						<div class="clr"></div>

					<?php else: ?>
						<?php $mainimage = get_field('property_main_image');

						if( !empty($mainimage) ): 

							// vars
							$url = $mainimage['url'];
							$alt = $mainimage['alt'];

							// img size
							$size = 'npt-property-main-img-size';
							$thumb = $mainimage['sizes'][ $size ];

						?>
								
						<?php endif; ?>
						<img src="<?php echo $thumb; ?>" alt="<?php echo $mainimage['alt']; ?>" class="mainimage" />
						<div class="clr"></div>
					<?php endif; ?>
					
				</div>
				</div>
			</div>
		</div>
	</article>
</main><!-- .content -->
	<div>
					
<?php endwhile; // end of the loop. ?>
	</div>
<?php get_footer(); ?>