<?php
/**
 * Template Name: Properties Page
 *
 * @package WordPress
 * @subpackage npt
 *
 */

get_header(); ?>
<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?> width-one-third" <?php omega_attr( 'content' ); ?>>
	<?php while ( have_posts() ) : the_post(); ?>
	<article <?php omega_attr( 'post' ); ?>>
		<div class="entry-wrap">
			<div class="entry-content" itemprop="text">
				<?php do_action( 'omega_before_entry' ); ?><!-- .page-header -->
				<?php the_content(); ?>

				<?php
				// get all the categories from the database
				//$cats = get_categories('taxonomy=npt_property_type');
				$cats = get_categories( array(
					'taxonomy' => 'npt_property_type',
					'orderby' => 'ID',
					'order'   => 'DESC'
				) );

                // loop through the categries
                foreach ($cats as $cat) {
                    // setup the cateogory ID
                    $cat_id= $cat->term_id;
                    // Make a header for the cateogry
                    echo "<h2>".$cat->name."</h2>";  
                    // create a custom wordpress query

                    $args = array(
						'tax_query' => array(
						array(
							'taxonomy' => 'npt_property_type',
							'field' => 'slug',
							'terms' => ".$cat->name."
							)
							)
						);
					$query = query_posts( $args );
                    // start the wordpress loop!
					?>
				<p>
                <?php    if (have_posts()) : while (have_posts()) : the_post(); ?>

                        <?php // create our link now that the post is setup ?>
                        <a href="<?php the_permalink();?>"><?php the_title(); ?></a><br/>

                    <?php endwhile; endif; // done our wordpress loop. Will start again for each category ?>
        		</p>
                    <?php wp_reset_query(); ?>
                <?php } // done the foreach statement ?>


			</div>
		</div>
	</article>
	<?php endwhile; // end of the loop. ?>
</main><!-- .content -->
	<div id="properties">
		<?php
			$args = array('post_type' => 'npt_properties');
			$loop = new WP_Query( $args );
			//Display the contents
			while ( $loop->have_posts() ) : $loop->the_post();
		?>

		<article class="properties-single entry">

				<?php $image = get_field('property_main_image');
					  $shorttitle = get_field('property_short_title');

				if( !empty($image) ): 

					// vars
					$url = $image['url'];
					$alt = $image['alt'];

					// img size
					$size = 'npt-property-main-img-size';
					$thumb = $image['sizes'][ $size ];

				?>
					
					<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>" class="properties-photo" style="background-image: url('<?php echo $thumb; ?>');">
						<?php $titlecolour = get_field('properties_title_colour'); ?>
						<h3 class="properties-name <?php if( !empty($titlecolour) & in_array('blue', $titlecolour) ) { echo 'blue'; } ?>"><?php echo $shorttitle; ?> <!--span class="arrow">></span--></h3>
					</a>	
				<?php else: ?>
					<a href="<?php echo get_permalink(); ?>" title="<?php the_title(); ?>" class="properties-photo" style="background-color: #318dde;">
						<?php $titlecolour = get_field('properties_title_colour'); ?>
						<h3 class="properties-name"><?php echo $shorttitle; ?> <!--span class="arrow">></span--></h3>
					</a>
				<?php endif; ?>
            
		</article>
		
		<?php endwhile;?>
					
	</div>

<?php get_footer(); ?>