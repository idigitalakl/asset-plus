<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage npt
 *
 */

get_header(); ?>
<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
	<?php 
	do_action( 'omega_before_content' ); 
	do_action( 'omega_content' );
	do_action( 'omega_after_content' ); 
	?>
<!--	<div class="npt-block width-50 share-price">-->
<!--		<div class="block-title entry-title">Share Price</div>-->
<!--		<div class="block-content">-->
<!--			--><?php
//			$xml=simplexml_load_file("http://feeds.companyresearch.nzx.com/live_share_price.php") or die("Error: Cannot create object");
//			/*print_r($xml);*/
//			echo $xml->feed->issuer->last;
//			?>
<!--		</div>-->
<!--	</div>-->
<!--	<div class="npt-block width-50 blue-net-bg">-->
<!--		<div class="block-title entry-title"><a href="--><?php //echo get_permalink(17); ?><!--" title="Investors">Investors</a></div>-->
<!--		<div class="block-content"><a href="--><?php //echo get_permalink(17); ?><!--" title="Investors"><img src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/images/line-chart.png" alt="Investors" class="img-line-chart" /></a></div>-->
<!--	</div>-->
</main><!-- .content -->
	<aside id="slideshow" class="<?php echo omega_apply_atomic( 'sidebar_class', 'slideshow col-2 widget-area' );?>" <?php omega_attr( 'sidebar' ); ?>>
<!--	<h3><a href="--><?php //echo get_permalink(15); ?><!--" title="Property Portfolio">Property<br/>Portfolio</a></h3>-->
                <div id="slider" class="">
                    <div class="carouselslider">
						<?php $images = get_field('slideshow_images');
						if( $images ): ?>
							<?php foreach( $images as $image ): ?>
							 <div>
								<div class="ssinside">
									<div class="ssinside-inner">
								<!--img src="<?php //echo $image['sizes']['medium']; ?>" alt="<?php //echo $image['alt']; ?>" /-->
								<div class="slideshow-main" style="background-image: url('<?php echo $image['sizes']['large']; ?>');">&nbsp;</div>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
						<?php endif; ?>
                    </div>	
                </div>
                
            <script type="text/javascript">
            //$(document).ready(function(){
            jQuery(document).ready(function($) {
              $('.carouselslider').slick({
                    dots: true,
					arrows: false,
                    infinite: true,
                    autoplay: true,
                    speed: 600,
                    autoplaySpeed: 3000,
                    slidesToShow: 1,
                    slidesToScroll: 1/*,
                  responsive: [
                    {
                      breakpoint: 480,
                      settings: {
                        slidesToShow: 1
                      }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                  ]*/
              });
              
            });
            </script>
  	</aside><!-- .slideshow -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>