<?php
/**
 * Template Name: Managers Page
 *
 * @package WordPress
 * @subpackage npt
 *
 */

get_header(); ?>
<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
	<?php 
	do_action( 'omega_before_content' ); 
	do_action( 'omega_content' ); 
	do_action( 'omega_after_content' ); 
	?>
	<div id="staff">
		<?php
			$args = array('post_type' => 'managers');
			$loop = new WP_Query( $args );
			//Display the contents
			while ( $loop->have_posts() ) : $loop->the_post();
		?>

		<article class="staff-single entry">
			<div class="staff-photo">
                <div class="staff-photo-wrapper">
                    <img class="staff-photo-square-helper" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAAAAAA6fptVAAAACklEQVR4nGP6DwABBQECz6AuzQAAAABJRU5ErkJggg=="/>

                        <?php $image = get_field('staff_photo');

                        if( !empty($image) ):

                            // vars
                            $url = $image['url'];
                            $alt = $image['alt'];

                            // img size
                            $size = 'npt-staff-photo-size';
                            $thumb = $image['sizes'][ $size ];

                        ?>

                        <img class="staff-actual-photo" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />

                        <?php endif; ?>

                </div>
            </div>
			<div class="staff-text">
				<h2 class="staff-name"><?php the_title(); ?></h2>
				<div class="staff-position"><?php the_field('staff_position'); ?></div>
				<?php the_content(); ?>
			</div>
		</article>
		
		<?php endwhile;?>
					
	</div>
</main><!-- .content -->
<?php get_footer(); ?>