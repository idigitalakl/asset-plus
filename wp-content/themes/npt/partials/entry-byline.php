<div class="entry-meta">
	<time <?php omega_attr( 'entry-published' ); ?>><?php echo get_the_date(); ?></time>
	<?php edit_post_link( __('Edit', 'omega'), ' | ' ); ?>
</div><!-- .entry-meta -->