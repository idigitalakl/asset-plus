<?php
/**
 * Template Name: Financial Reporting Page
 *
 * @package WordPress
 * @subpackage npt
 *
 */

get_header(); ?>
<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php omega_attr( 'post' ); ?>>
		<div class="entry-wrap">
			<div class="entry-content" itemprop="text">
				<?php do_action( 'omega_before_entry' ); ?><!-- .page-header -->
				<?php the_content(); ?>



				<?php
					// check if the repeater field has rows of data
					if( have_rows('reports_by_year') ):
				?>
				<div class="financial-reports">
				<!--h2>Financial Reports</h2-->
					<?php
					  // loop through the rows of data
					  while ( have_rows('reports_by_year') ) : the_row();
						$reports_year = get_sub_field('reports_year');
						?>
						<div class="reports-single">
						  <h3><?php echo $reports_year; ?></h3>
							<?php
								$interim_report_file = get_sub_field('interim_report_file');
								$interim_report_image = get_sub_field('interim_report_image');
								$interim_report_url = $interim_report_file["url"];
								if( $interim_report_file ) {
							?>
								  <p class="interim-report">
								  <a href="<?php echo $interim_report_url; ?>" title="<?php echo 'NPT Limited '.$reports_year.' Interim Report'; ?>" target="_blank">
									<?php	if( !empty($interim_report_image) ): 

											$thumb = $interim_report_image['sizes'][ 'medium' ];

										?>

										Interim Report<br/> 
										<img src="<?php echo $thumb; ?>" alt="<?php echo 'NPT Limited '.$reports_year.' Interim Report'; ?>" />
												
										<?php endif; ?>
								  </a>
								  </p>
							<?php } ?>
							<?php
								$financial_statements_file = get_sub_field('financial_statements_file');
								$financial_statements_image = get_sub_field('financial_statements_image');
								$financial_statements_url = $financial_statements_file["url"];
								if( $financial_statements_file ) {
							?>
								  <p class="interim-report financial-statements">
								  <a href="<?php echo $financial_statements_url; ?>" title="<?php echo 'NPT Limited '.$reports_year.' Financial Statements'; ?>" target="_blank">
									<?php	if( !empty($financial_statements_image) ): 

											$thumb = $financial_statements_image['sizes'][ 'medium' ];

										?>

										Financial Statements<br/> 
										<img src="<?php echo $thumb; ?>" alt="<?php echo 'NPT Limited '.$reports_year.' Financial Statements'; ?>" />
												
										<?php endif; ?>
								  </a>
								  </p>
							<?php } ?>
							<?php
								$annual_report_file = get_sub_field('annual_report_file');
								$annual_report_image = get_sub_field('annual_report_image');
								$annual_report_url = $annual_report_file["url"];
								if( $annual_report_file ) {
							?>
								  <p class="annual-report">
								  <a href="<?php echo $annual_report_url; ?>" title="<?php echo 'NPT Limited '.$reports_year.' Annual Report'; ?>" target="_blank">
									<?php	if( !empty($annual_report_image) ): 

											$thumb = $annual_report_image['sizes'][ 'medium' ];

										?>

										Annual Report <br/>
										<img src="<?php echo $thumb; ?>" alt="<?php echo 'NPT Limited '.$reports_year.' Annual Report'; ?>" />
												
										<?php endif; ?>
								  </a>
								  </p>
							<?php } ?>
						<div class="clr"></div>
						</div>
						
					 <?php endwhile; ?>
				<div class="clr"></div>
				</div>
					<?php endif; ?>
		
		

				<?php
					// check if the repeater field has rows of data
					if( have_rows('agm_presentation') ):
				?>
				<div class="investor-presentations">
				<h2>Annual Meeting Presentations</h2>
					<?php
					  // loop through the rows of data
					  while ( have_rows('agm_presentation') ) : the_row();
						$agm_presentation_year = get_sub_field('agm_presentation_year');
						$agm_presentation_image = get_sub_field('agm_presentation_image');
						$agm_presentation_file = get_sub_field('agm_presentation_file');
						$agm_presentation_file_url = $agm_presentation_file["url"];
						if( $agm_presentation_file ) { 
					?>
						<div class="presentation-single">
						  <h3><?php echo $agm_presentation_year; ?></h3>
						  <p>
						  <a href="<?php echo $agm_presentation_file_url; ?>" title="<?php echo 'NPT Limited '.$agm_presentation_year.' Annual Meeting Presentation'; ?>" target="_blank">
							<?php	if( !empty($agm_presentation_image) ): 
									
									//$agm_presentation_image_url = $agm_presentation_image['url']; // img url
									$agm_presentation_image_thumb = $agm_presentation_image['sizes'][ 'medium' ];// img size

								?>

								Annual Meeting Presentation<br/>
								<img src="<?php echo $agm_presentation_image_thumb; ?>" alt="<?php echo 'NPT Limited '.$agm_presentation_year.' Annual Meeting Presentation'; ?>" />

								<?php endif; ?>
						  </a>
						  </p>
						</div>
					<?php
						}
							//view all data values
						//var_dump($agm_presentation_file);
					 endwhile; ?>
					 <div class="clr"></div>
				</div>
					<?php endif; ?>
		
		

				<?php
					// check if the repeater field has rows of data
					if( have_rows('investor_presentations_by_year') ):
				?>
				<div class="investor-presentations">
				<h2>Investor Presentations</h2>
					<?php
					  // loop through the rows of data
					  while ( have_rows('investor_presentations_by_year') ) : the_row();
						$investor_presentation_year = get_sub_field('investor_presentation_year');
						?>
						<div class="reports-single">
						  <h3><?php echo $investor_presentation_year; ?></h3>
							<?php
								$interim_investor_presentation_image = get_sub_field('interim_investor_presentation_image');
								$interim_investor_presentation_file = get_sub_field('interim_investor_presentation_file');
								$interim_investor_presentation_file_url = $interim_investor_presentation_file["url"];
								if( $interim_investor_presentation_file ) {
							?>
								  <p class="interim-report">
								  <a href="<?php echo $interim_investor_presentation_file_url; ?>" title="<?php echo 'NPT Limited '.$investor_presentation_year.' Interim Investor Presentation'; ?>" target="_blank">
									<?php	if( !empty($interim_investor_presentation_image) ): 

											$interim_investor_presentation_image_thumb = $interim_investor_presentation_image['sizes'][ 'medium' ];

										?>

										Interim Investor Presentation<br/> 
										<img src="<?php echo $interim_investor_presentation_image_thumb; ?>" alt="<?php echo 'NPT Limited '.$investor_presentation_year.' Interim Investor Presentation'; ?>" />
												
										<?php endif; ?>
								  </a>
								  </p>
							<?php } ?>
							<?php
								$annual_investor_presentation_image = get_sub_field('annual_investor_presentation_image');
								$annual_investor_presentation_file = get_sub_field('annual_investor_presentation_file');
								$annual_investor_presentation_file_url = $annual_investor_presentation_file["url"];
								if( $annual_investor_presentation_file ) {
							?>
								  <p class="annual-report">
								  <a href="<?php echo $annual_investor_presentation_file_url; ?>" title="<?php echo 'NPT Limited '.$investor_presentation_year.' Annual Investor Presentation'; ?>" target="_blank">
									<?php	if( !empty($annual_investor_presentation_image) ): 

											$annual_investor_presentation_image_thumb = $annual_investor_presentation_image['sizes'][ 'medium' ];

										?>

										Annual Investor Presentation<br/> 
										<img src="<?php echo $annual_investor_presentation_image_thumb; ?>" alt="<?php echo 'NPT Limited '.$investor_presentation_year.' Annual Investor Presentation'; ?>" />
												
										<?php endif; ?>
								  </a>
								  </p>
							<?php } ?>
						<div class="clr"></div>
						</div>
						
					 <?php endwhile; ?>
				<div class="clr"></div>
				</div>
					<?php endif; ?>
		
			</div>
			
		</div>
	</article>
<?php endwhile; // end of the loop. ?>
</main><!-- .content -->

<?php get_footer(); ?>