<?php
//include parent css style
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

// Remove parent theme Credit from Footer
/*
function custom_remove_footer_credit () {
	remove_filter( 'omega_footer_insert', 'omega_default_footer_insert' );
    add_action( 'omega_footer_insert', 'custom_add_footer_credit');
} 
add_action( 'init', 'custom_remove_footer_credit');
function custom_add_footer_credit() {
	echo '<p class="copyright">&copy; 2002 - ' . date("Y") . ' ' . get_bloginfo( 'name' ) . '. All rights reserved.</p>' . "\n\n" . '<p class="credit"><a class="child-link" href="http://www.ascona.co.nz/web" target="_blank" title="Ascona -  One stop shop for web design and hosting">Web by Ascona</a></p>';
}
*/


/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function custom_theme_setup() {
	
	/* Load the primary menu. */
	remove_action( 'omega_before_header', 'omega_get_primary_menu' );	
	add_action( 'omega_header', 'omega_get_primary_menu' );

	// Remove parent theme Credit from Footer and add a custom one
	remove_filter( 'omega_footer_insert', 'omega_default_footer_insert' );
    add_action( 'omega_footer_insert', 'custom_add_footer_credit');

	// Remove sidebar
	remove_action( 'omega_after_main', 'omega_primary_sidebar' );
}

add_action( 'after_setup_theme', 'custom_theme_setup', 11 );

function custom_add_footer_credit() {
	echo '<p class="copyright">&copy; ' . get_bloginfo( 'name' ) . '. All rights reserved.</p>';
}




// Add Google fonts
function wpse_google_webfonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    $query_args = array(
        'family' => 'Poppins:400,600|Tinos:400,700|Montserrat:400,500,600,700',
        'subset' => 'latin,latin-ext',
    );

    wp_enqueue_style('google-webfonts',
        add_query_arg($query_args, "$protocol://fonts.googleapis.com/css" ),
        array(), null);
}

add_action( 'wp_enqueue_scripts', 'wpse_google_webfonts' );

// Slick carousel scripts
/*
function npt_enqueue_front_page_scripts() {
    //if( is_front_page() )
    //{
        wp_enqueue_style( 'slick', get_bloginfo('template_url').'/slick/slick.css', array( 'css' ), null, true );
        wp_enqueue_style( 'slickcss', get_bloginfo('template_url').'/slick/slick-theme.css', array( 'css' ), null, true );
    //}
}
add_action( 'wp_enqueue_scripts', 'npt_enqueue_front_page_scripts' );
*/


// Load the theme stylesheets
function npt_enqueue_scripts()  
{ 

	// Example of loading a jQuery slideshow plugin just on the homepage
	//wp_register_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css' );

	// Load all of the styles that need to appear on all pages

	// Conditionally load the FlexSlider CSS on the homepage
	if( is_page('home') || is_singular('npt_properties') ) {
	//	wp_enqueue_style('flexslider');
	wp_enqueue_script( 'defaultjquery', '/wp-includes/js/jquery/jquery.js');
	wp_enqueue_script( 'defaultjquerymigrate', '/wp-includes/js/jquery/jquery-migrate.js');
	wp_enqueue_script( 'slickminjs', get_stylesheet_directory_uri() . '/slick/slick.min.js');
	wp_enqueue_style( 'slickcss', get_stylesheet_directory_uri() . '/slick/slick.css' );
	wp_enqueue_style( 'slickthemecss', get_stylesheet_directory_uri() . '/slick/slick-theme.css');
	}

}
add_action('wp_enqueue_scripts', 'npt_enqueue_scripts');


//************************************************************
// Custom post type for the Board of Directors
//************************************************************

function create_posttype_directors() {

	register_post_type( 'directors',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Directors' ),
				'singular_name' => __( 'Director' ),
				'menu_name' => __( 'Directors' )
			),
			'public' => true,
			'supports'      => array( 'title', 'editor'/*, 'thumbnail', 'excerpt'*/ ),
			//'has_archive' => true,
			//'rewrite' => array('slug' => 'header-contacts'),
			'menu_icon' => 'dashicons-businessman'
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype_directors' );


//************************************************************
// Custom post type for the Executive Management
//************************************************************

function create_posttype_managers() {

	register_post_type( 'managers',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Managers' ),
				'singular_name' => __( 'Manager' ),
				'menu_name' => __( 'Managers' )
			),
			'public' => true,
			'supports'      => array( 'title', 'editor'/*, 'thumbnail', 'excerpt'*/ ),
			//'has_archive' => true,
			//'rewrite' => array('slug' => 'header-contacts'),
			'menu_icon' => 'dashicons-groups'
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype_managers' );


//************************************************************
// Custom post type for the Properties
//************************************************************

function create_posttype_properties() {

	register_post_type( 'npt_properties',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Properties' ),
				'singular_name' => __( 'Property' ),
				'menu_name' => __( 'Properties' )
			),
			'public' => true,
			'supports'      => array( 'title', 'editor'/*, 'thumbnail', 'excerpt'*/ ),
			//'taxonomies' => array( 'topics', 'category' ),
			//'has_archive' => true,
			'rewrite' => array('slug' => 'property-portfolio/%npt_property_type%'),
			'menu_icon' => 'dashicons-building'
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype_properties' );

function npt_taxonomies_property() {
  $labels = array(
    'name'              => _x( 'Property Types', 'taxonomy general name' ),
    'singular_name'     => _x( 'Property Type', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Property Types' ),
    'all_items'         => __( 'All Property Types' ),
    //'parent_item'       => __( 'Parent Product Category' ),
    //'parent_item_colon' => __( 'Parent Product Category:' ),
    'edit_item'         => __( 'Edit Property Type' ), 
    'update_item'       => __( 'Update Property Type' ),
    'add_new_item'      => __( 'Add New Property Type' ),
    'new_item_name'     => __( 'New Property Type' ),
    'menu_name'         => __( 'Property Types' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
        'rewrite' => array(
            //'slug' => 'types')
            'slug' => 'property-portfolio')
            //'slug' => 'events/%event%',
            //'with_front' => false
	);
  register_taxonomy( 'npt_property_type', 'npt_properties', $args );
}

add_action( 'init', 'npt_taxonomies_property', 0 );

function filter_post_type_link($link, $post)
{
    if ($post->post_type != 'npt_properties')
        return $link;

    if ($cats = get_the_terms($post->ID, 'npt_property_type'))
        $link = str_replace('%npt_property_type%', array_pop($cats)->slug, $link);
    return $link;
}
add_filter('post_type_link', 'filter_post_type_link', 10, 2);



/* ------------ Custom Image sizes ------------ */
add_image_size( 'npt-tiny', 150, 150, false );
add_image_size( 'npt-staff-photo-size', 600, 650, true );
add_image_size( 'npt-property-main-img-size', 800, 600, true );

//Ninja Forms move error message to the bottom
function custom_move_ninja_forms_messages() {
    remove_action( 'ninja_forms_display_before_form', 'ninja_forms_display_response_message', 10 );
    add_action( 'ninja_forms_display_after_form', 'ninja_forms_display_response_message', 10 );
}
add_action( 'wp_head', 'custom_move_ninja_forms_messages' );

?>