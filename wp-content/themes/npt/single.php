<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Omega
 */

get_header(); ?>

<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php omega_attr( 'post' ); ?>>
		<div class="entry-wrap">
			<div class="entry-content" itemprop="text">
                <a href="/latest-news" class="backToNews">&laquo; Back to Latest News</a>
				<?php do_action( 'omega_before_entry' ); ?><!-- .page-header -->
				<?php the_content(); ?>




<?php
    // check if the repeater field has rows of data
    if( have_rows('attachments') ):
?>
<h3>Attachments</h3>
<?php
      // loop through the rows of data
      while ( have_rows('attachments') ) : the_row();
        $filename = get_sub_field('attachment_name');
        $uploaded_file = get_sub_field('uploaded_file');
        $title = $uploaded_file["title"];
        $url = $uploaded_file["url"];
        if( $uploaded_file ) { 
?>
          <p><a href="<?php echo $url; ?>" title="<?php echo $filename; ?>" target="_blank"><?php echo $filename; ?></a></p>       
<?php
        }
            //view all data values
        //var_dump($uploaded_file);
     endwhile;
    else :
        // no rows found
    endif;
?>
			</div>
		</div>
	</article>
<?php endwhile; // end of the loop. ?>
</main><!-- .content -->
					
<?php get_footer(); ?>