<?php
/**
 * Template Name: Wide Sidebar
 *
 * @package WordPress
 * @subpackage npt
 *
 */

get_header(); ?>
<main  class="<?php echo omega_apply_atomic( 'main_class', 'content' );?>" <?php omega_attr( 'content' ); ?>>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php omega_attr( 'post' ); ?>>
		<div class="entry-wrap">
			<div class="entry-content" itemprop="text">
				
				<div class="wide-sidebar-left"><?php do_action( 'omega_before_entry' ); ?><!-- .page-header --><?php the_content(); ?></div>
				
				<div class="wide-sidebar-right" role="complementary" itemscope="" itemtype="http://schema.org/WPSideBar">	
					<div class="wide-sidebar-content">
						<?php the_field('wide_sidebar'); ?>
					</div>
				</div>
				
			</div>
		</div>
	</article>
<?php endwhile; // end of the loop. ?>
</main><!-- .content -->
<?php get_footer(); ?>