<nav id="navigation" class="nav-primary" <?php omega_attr( 'menu' ); ?>>	
	<?php 
	do_action( 'omega_before_primary_menu' ); 
	wp_nav_menu( array(
		'theme_location' => 'primary',
		'container'      => '',
		'menu_class'     => 'menu omega-nav-menu menu-primary',
		'fallback_cb'	 => 'omega_default_menu'
		)); 
	do_action( 'omega_after_primary_menu' );
	?>
	<div class="sharePrice">
		<h2>Share Price</h2>
		<p>$<?php $xml=simplexml_load_file("https://feeds.companyresearch.nzx.com/live_share_price.php?stock=APL"); if(isset($xml->feed)) echo $xml->feed->issuer->last; ?></p>
	</div>
</nav><!-- .nav-primary -->