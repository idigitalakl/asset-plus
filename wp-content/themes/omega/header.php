<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> <?php omega_attr( 'body' ); ?>>
<?php do_action( 'omega_before' ); ?>
<div class="<?php echo omega_apply_atomic( 'site_container_class', 'site-container' );?>">

    <section class="home-full-screen-image" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/images/splash_bg.jpg)">
        <div class="home-full-screen-image-wrapper">
            <img class="logo-home" itemprop="logo" alt="Asset Plus by Augusta" src="<?php echo get_stylesheet_directory_uri() ?>/images/logo_white.svg"/>
            <p>A new vision, a new strategy, a new and passionate set of experts...</p>
            <p>The launch of Asset Plus and its journey to a brighter future.</p>
            <a href="/our-company" class="openAssetPlusVideo">Watch the video <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M0 0h24v24H0z" fill="none"/>
                    <path d="M10 16.5l6-4.5-6-4.5v9zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"/>
                </svg>
            </a>
        </div>

        <a href="#content" class="continueToSite">Continue</a>
    </section>

    <a href="#" class="mobileMenuButton">Open Menu</a>
    <div class="mobileMenu">
        <a href="#" class="mobileMenuCloseButton">Open Menu</a>
        <?php wp_nav_menu([ 'menu' => 'Main Menu'] ) ?>
    </div>

	<?php 
	do_action( 'omega_before_header' );
	do_action( 'omega_header' );
	do_action( 'omega_after_header' ); 
	?>
	<div class="site-inner">
		<?php do_action( 'omega_before_main' ); ?>