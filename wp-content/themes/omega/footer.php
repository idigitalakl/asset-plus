<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the class=site-inner div and all content after
 *
 * @package Omega
 */
?>
		<?php do_action( 'omega_after_main' ); ?>
	</div><!-- .site-inner -->
	<?php 
	do_action( 'omega_before_footer' ); 
	do_action( 'omega_footer' ); 
	do_action( 'omega_after_footer' ); 
	?>
</div><!-- .site-container -->
<?php do_action( 'omega_after' ); ?>
<?php wp_footer(); ?>
<script>
    jQuery(document).ready(function($) {
        $(window).on('scroll touchmove', function () {
            $('#header').toggleClass('not-top', $(document).scrollTop() > 10);
		}).scroll();
		var $mobileMenu = $('.mobileMenu');
		$(".mobileMenuButton").on('click', function(){
			$mobileMenu.fadeIn(300);
			return false;
		})
		$(".mobileMenuCloseButton").on('click', function(){
			$mobileMenu.fadeOut(300);
			return false;
		})
    });

</script>
</body>
</html>