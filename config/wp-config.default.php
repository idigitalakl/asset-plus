<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file.
 * 
 * Please note if you add constants in this file (i.e. define statements) 
 * these cannot be overridden in environment config files so make sure these are only set once.
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    2.0.0
 * @author     Studio 24 Ltd  <hello@studio24.net>
 */
  

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Dfw57SWa8GlnQWaaCvSIPCPq56O0W55DZ09Xp0e8e3gwBZebiWAVSdX4GXHbsPtf');
define('SECURE_AUTH_KEY',  'ewdykG5HUKUy576OnJmovx2RZ5GhkpVMH5KKVoXOgIByUOnUDVz8RdWE5hcalZOY');
define('LOGGED_IN_KEY',    'uqOnge4sRKJgRTTI4hrCNW9MuNzpGfqj7kaQaOUaRifhSXYit2qwjJECCOHKfVPn');
define('NONCE_KEY',        'Bs3PB80zARm5hDTYvzg6tCzvB1wJhsSNjML6oJ7pLKr0QHL48J2kh6JxY6UUUw9Q');
define('AUTH_SALT',        'CHyguSymnRlbjYDh2IQAXmVR4EqORwQuXXnzxYf8K7gFhAQqFvr6zSmKkP2jOuYx');
define('SECURE_AUTH_SALT', '843JYayZpKuXaSY4e5h56n0RQf1hLUnlWmVO6oq2RQ9QeC7NdScyXyz8zpYZcPwx');
define('LOGGED_IN_SALT',   'tdHtzWLEGbnxHw5ACNFyG39egV7HCKa3mG8qmMCguOZQJ7347XxQwBBBrEcs3D6t');
define('NONCE_SALT',       'BTK8PlIKxCOBdA3khdCVCjha1KSKifMHhdcaj4nkHlfs5xIzymDkrvpiFYOm0snn');

/**#@-*/

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'nptopi_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * Increase memory limit. 
 */
define('WP_MEMORY_LIMIT', '64M');
